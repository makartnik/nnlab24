﻿using System;
using System.Linq;

namespace NNLab24
{
    public class Candidate
    {
        public Neuron[] HiddenNeurons { get; }

        public Neuron OutputNeuron { get; }

        public double Error { get; set; }

        public Candidate(int inputCount, int hiddenCount, int seed)
        {
            var random = new Random(seed);
            HiddenNeurons = Enumerable.Range(random.Next(), hiddenCount)
                .Select(s => new Neuron(inputCount, s))
                .ToArray();
            OutputNeuron = new Neuron(hiddenCount, hiddenCount);
        }
    }
}