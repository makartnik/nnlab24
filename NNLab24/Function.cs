﻿using System;

namespace NNLab24
{
    public class Function
    {
        public double GetValue(double x)
        {
            return Math.Sin(x) * Math.Tan(x);

            //3 * x * Math.Exp(Math.Cos(x));
        }
    }
}