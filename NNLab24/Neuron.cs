﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NNLab24
{
    public class Neuron
    {
        public double[] Weights { get; set; }

        public Neuron(int size, int seed)
        {
            Weights = new double[size];
            Random random = new Random(seed);
            for (int i = 0; i < size; i++)
                Weights[i] = random.NextDouble() * 2 - 1;
        }

        public double F(double[] inputData)
        {
            if (inputData.Length != Weights.Length)
                throw new ArgumentException(nameof(inputData));
            return inputData.Select((v, i) => v * Weights[i]).Sum();
        }
    }
}