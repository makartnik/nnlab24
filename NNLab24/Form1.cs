﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace NNLab24
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            zedGraphControl1.GraphPane.Title.IsVisible = false;
            //zedGraphControl1.GraphPane.XAxis.Title.IsVisible =
            //    zedGraphControl1.GraphPane.YAxis.Title.IsVisible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var func = new Function();
            var network = new NeuralNetwork();
            network.Train(start: 0,
                count: 10,
                iterationCount: 100,
                populationSize: 30,
                inputCount: 10,
                hiddenCount: 5,
                seed: 636778);

            var pane = zedGraphControl1.GraphPane;
            pane.CurveList.Clear();

            const int outSize = 4;

            pane.AddCurve("sin(x) * tg(x)",
                Enumerable.Range(0, 10 + outSize).Select(x => (double) x).ToArray(),
                Enumerable.Range(0, 10 + outSize).Select(x => func.GetValue(x)).ToArray(),
                Color.Red,
                SymbolType.Circle);

            var input = Enumerable.Range(0, 10)
                .Select(x => func.GetValue(x))
                .ToList();

            List<double> args = new List<double>(),
                vals = new List<double>();

            for (int i = 0; i < 10; ++i)
            {
                var inp = Enumerable.Range(i - 10, 10).Select(a => (double) func.GetValue(a)).ToArray();
                double x = i;
                double y = network.GetValue(inp);
                args.Add(x);
                vals.Add(y);
            }


            for (int i = 0; i < outSize; ++i)
            {
                double x = i + 10;
                double y = network.GetValue(input.ToArray());
                input.Add(y);
                input = input.Skip(1).ToList();
                args.Add(x);
                vals.Add(y);
            }

            pane.AddCurve("Neural",
                args.ToArray(),
                vals.ToArray(),
                Color.Blue,
                SymbolType.Square);

            zedGraphControl1.AxisChange();

            pane.AddCurve(label: "",
                x: new[] {9.5, 9.5},
                y: new[] {-1000000.0, 1000000.0},
                color: Color.Black);

            zedGraphControl1.Invalidate();
        }
    }
}