﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NNLab24
{
    public class NeuralNetwork
    {
        private List<Candidate> _population;

        private Random _random;

        private Function _function = new Function();

        private List<Candidate> GetNextPopultion(List<Candidate> currentPopulation, Random random, int start, int count)
        {
            var nextPopulation = new List<Candidate>();

            int currentSize = currentPopulation.Count;
            for (int i = 0; i < currentSize - 1; i++)
            {
                for (int j = i + 1; j < currentSize; j++)
                {
                    var children = new Candidate(currentPopulation[i].HiddenNeurons[0].Weights.Length,
                        currentPopulation[i].HiddenNeurons.Length, 0);
                    for (int k = 0; k < children.HiddenNeurons.Length; k++)
                    {
                        var neuron = children.HiddenNeurons[k];
                        neuron.Weights = neuron.Weights.Select((weight, index) => random.Next() % 2 == 0
                                ? currentPopulation[i].HiddenNeurons[k].Weights[index]
                                : currentPopulation[j].HiddenNeurons[k].Weights[index])
                            .ToArray();
                    }

                    children.OutputNeuron.Weights = children.OutputNeuron.Weights.Select(
                            (weight, index) => random.Next() % 2 == 0
                                ? currentPopulation[i].OutputNeuron.Weights[index]
                                : currentPopulation[j].OutputNeuron.Weights[index])
                        .ToArray();

                    int indexToChange = random.Next(0, children.OutputNeuron.Weights.Length - 1);
                    children.OutputNeuron.Weights[indexToChange] *= random.NextDouble() * 4 - 2;

                    foreach (var neuron in children.HiddenNeurons)
                    {
                        indexToChange = random.Next(0, neuron.Weights.Length - 1);
                        neuron.Weights[indexToChange] *= random.NextDouble() * 4 - 2;
                    }
                    children.Error = 0;
                    for (int l = 0; l < count; ++l)
                    {
                        children.Error += GetError(children, start - l, count);
                    }
                   
                    nextPopulation.Add(children);
                }
            }
            return nextPopulation;
        }

        private double GetError(Candidate candidate, int start, int count)
        {
            var inputData = Enumerable
                .Range(start, count)
                .Select(x=>_function.GetValue(x))
                .ToArray();

            return Math.Pow(_function.GetValue(start + count) - GetValue(inputData, candidate), 2);
        }

        public void Train(int start, int count, int iterationCount, int populationSize, int inputCount, int hiddenCount,
            int seed = 0)
        {
            _random = new Random(seed);
            _population = Enumerable.Range(_random.Next(), populationSize)
                .Select(s =>
                {
                    var candidate = new Candidate(inputCount, hiddenCount, s);
                    candidate.Error = 0;
                    for (int l = 0; l < count; ++l)
                    {
                        candidate.Error += GetError(candidate, start - l, count);
                    }
                    return candidate;
                })
                .ToList();
            for (int i = 0; i < iterationCount; i++)
            {
                _population = GetNextPopultion(_population, _random, start, count)
                    .OrderBy(c=>c.Error)
                    .Take(populationSize)
                    .ToList();
            }
        }


        public double GetValue(double[] inputData, Candidate candidate)
        {
            var hiddenOutput = candidate.HiddenNeurons.Select(n => n.F(inputData)).ToArray();
            return candidate.OutputNeuron.F(hiddenOutput);
        }

        public double GetValue(double[] inputData)
        {
            return GetValue(inputData, _population.First());
        }
    }
}